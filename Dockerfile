FROM gaah/shiny-server:latest

RUN ["mkdir", "R"]
RUN ["R", "-e", \
    "dir.create(Sys.getenv('R_LIBS_USER'), showWarnings = FALSE, recursive = TRUE);\
    install.packages(c('ggplot2'), Sys.getenv('R_LIBS_USER'), repos = 'https://cloud.r-project.org')"]
RUN ["git", "clone", "https://gcaillaut@bitbucket.org/gcaillaut/hello-shiny.git", "apps/hello-shiny"]
